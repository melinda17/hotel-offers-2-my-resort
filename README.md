# Hotel-offers application:

App name: (ToBeDefined)
Team name: CodeFighters
Team members:
	Jónás Dávid
	Fodor Krisztián
	Tóth Melinda
	Kanalas Richárd
	Pusztai Tamás


Welcome to our gitlab project name is (ToBeDefined). 

We are students of Progmatic Academy and currently we are working on our final project of this front-end developer course.

This application is a Hotel Room application where the visitor (User) could filter and select based on some conditions the best accomodation offer and reserve it. 


Furthermore can save the favourite offers and next time can filter for them to review again before choose the best option. 

User can evaluate the hotels based on their experiences at the apartment.


Hotel owners (Provider) can register on the platform with their profile, upload and edit offers and follow up the incoming orders from the Users.



