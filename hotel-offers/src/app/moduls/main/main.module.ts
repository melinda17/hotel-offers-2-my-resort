import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Overview } from './overview/overview-container/overview';
import { FeaturedItemsComponent } from './overview/featured-items/featured-items.component';
import { OffersContainerComponent } from './offers-view/offers-container/offers-container.component';
import { OfferDetailComponent } from './offer-detail-view/offer-detail/offer-detail.component';
import { OfferItemComponent } from './offers-view/offer-item/offer-item.component';
import { MainComponent } from './main.component';



@NgModule({
  declarations: [
    Overview,
    FeaturedItemsComponent,
    OffersContainerComponent,
    OfferDetailComponent,
    OfferItemComponent,
    MainComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MainModule { }
